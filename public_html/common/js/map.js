/**
 * Created by Mario on 10/1/2017.
 */

//**GOOGLE MAP**//


window.onload = function () {

    var mapOptions = {
        center: new google.maps.LatLng(33.571381, 133.536008),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    // document.getElementById("map").style.height = $(window).height()+'px';
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var marker = new google.maps.Marker({
        position: {
            lat:33.571381,
            lng:133.536008
        },
        map:map
    });
};