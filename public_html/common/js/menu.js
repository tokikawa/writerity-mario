/**
 * Created by Mario on 10/1/2017.
 */


//** Toggle Menu When In Small Size **//

$('.hamburger, .hide').on('click',function () {
    $('.helpmenu').toggleClass('openned');
    $('.content').toggleClass('opennedContent');
});

//** Menu links to scroll smoothly **//

$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top-90
    },900,"swing");
    return false;
});

//** Go to top smoothly **//

$('#toTop').click(function(){
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
});

//** To-Top button to appear**//
window.onscroll = function() {
    if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
        $('#toTop').fadeIn();
    } else {
        $('#toTop').fadeOut();
    }
};