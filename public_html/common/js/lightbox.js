/**
 * Created by Mario on 10/1/2017.
 */

// //** Lightbox **//

$(document).ready(function () {
    $(".lightBox").click(function () {
        $src=$(this).attr("src");
        if(!$("#light-box").length>0){
            $("body").append("<div id='light-box'><img src=''></div>");
            $("#light-box").show();
            $("#light-box img").attr("src",$src);
        }else{
            $("#light-box").show();
            $("#light-box img").attr("src",$src);
        }
    });

    $("body").on("click","#light-box", function () {
        $("#light-box").hide();
    });
});


